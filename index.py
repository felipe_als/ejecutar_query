from  app import config #ejecuta la configuración
from  app.modelo.Excel import *
from  app.conexion.Mysql import *
from  app.modelo.Mail import *
from os import listdir#se ocupa para obtener los archivos de una carpeta
from os.path import isfile, join#se ocupa para obtener los archivos de una carpeta
from os import system#limpia la consola




def listar():
	archivos = []
	ruta =config._QUEY_RUTA
	for arch in listdir(ruta): 
		if isfile(join(ruta, arch)):
			archivos.append(arch)

	return archivos

def getQuery(queryfile):
    sql_file = config._QUEY_RUTA + queryfile
    print(sql_file)
    file = open(sql_file, 'r')
    #sql = file.read().replace('\n', ' ')
    sql = file.read()
    return sql

def clear():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

def consola(texto=True):
	if texto:
		print("Presione cualquier tecla para continuar")

	print("'exit' para salir")
	cadena=input(">>>")

	if(cadena.strip()=="exit"):
		exit()
	return cadena



while True:  
	clear()
	archivos=listar()
	if len(archivos)==0:
		print("Sin Query en "+config._QUEY_RUTA)
		exit()

	print("_________________________________________________")
	print("Ingrese el número de la query que desea ejecutar:")
	print("Si es más de una, separelo por coma (ej: 1,2)\n")

	
	for indice, nombre in enumerate(archivos):
		print(str(indice+1)+" - "+nombre.replace(".sql",""))

	
	print("_________________________________________________")
	cadena=consola(False)

	consultas = cadena.split(",")



	if(len(consultas)>0):

		
		archivos_creados=[]
		for n in consultas:
			

			try:
				n= int(n.strip())
			except Exception as e:
				print("'"+n+"' no es una opción válida")
				consola()
				continue

			if n<=0 or n>len(archivos):
				continue

			con = Mysql()#crea una conexión

			nom_archivo=archivos[int(n)-1]#obtiene el nombre del archivo seleccionado



			sql=getQuery(nom_archivo)#obtiene la sentencia sql

			listas = con.getConEncabezado(sql)#retorna una lista donde la primera fila es el nombre de las columnas
			if listas==None:
				print("_________________________________________________")
				print(sql)
				print("_________________________________________________")
				consola()

			
			doc=Excel().generar(listas,nombre=archivos[int(n)-1].replace(".sql",""),nombre_hoja="Reporte")#Crea un excel con la lista insertada y retorna la ruta del archivo

			archivos_creados.append(doc);
		
		clear()

		if len(archivos_creados)>0:
			print("Se han creado los siguientes reportes:")
			print(archivos_creados)
			print("desea enviarlos por email a:")
			if config._APP_ENV=="TEST":
				print(config.DAFAULT_ADDRESS)
			else:
				print(config.LISTA_DESTINARIOS)
			print("[s/n] ??")

			cadena=input(">>>")

			if(cadena.strip()[0]=="s"):
				mail = Mail()
				print("Email: Asunto:")
				asunto=input(">>>")
				print("Email: mensaje:")
				mensaje=input(">>>")
				print("Enviando...")
				mail.enviar(asunto=asunto,texto=mensaje,archivos=archivos_creados)
				clear()
				print("Correo enviado con "+str(len(archivos_creados))+" archivo(s)")
				
		else:
			print("0 archivos creados")
		
		consola()






