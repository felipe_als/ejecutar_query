from  app.modelo.Excel import *
from  app.conexion.Mysql import *
from  app.modelo.Mail import *

class EstadoSituacionIngles:
	
	def __init__(self):
		pass


	def ejecutar(self,enviar_mail=False):
		con = Mysql()
		#retorna una lista donde la primera fila es el nombre de las columnas
		listas = con.getConEncabezado(self.getSql())
		#Crea un excel con la lista insertada y retorna la ruta del archivo
		doc=Excel().generar(listas,nombre="estado_situacion_ingles",nombre_hoja="reporte")
		#crea una nueva instancia de Mail
		mail = Mail()
		#envia un mail con el archivo adjunto
		if enviar_mail :
			mail.enviar(asunto="Prueba",texto="este es el texto",archivos=[doc])

	def getSql(self):

		sql =   """
					select ra_facultad.descripcion as descripcion_facultad,									
					mt_tipocarr.descripcion as descripcion_tipo_carrera,										
					cast(mt_carrer.codcarr as varchar(30)) + ' - ' + mt_carrer.nombre_l as DESCRIPCION_CARRERA,
					mt_carrer.jornada,												--JORANADA
					mt_alumno.codcli as codigo_matricula,												--CODIGO ALUMNO
					mt_alumno.rut,													--RUT
					mt_client.dig,													--DV
					(cast(mt_client.paterno as varchar(100)) + ' ' + cast(mt_client.materno as varchar(100))+ ' ' + cast(mt_client.nombre as varchar(100))) AS nombre_estudiante,												--NOMBRE
					CONVERT(VARCHAR(10), mt_client.fecnac, 103) as fecha_nacimiento,--FECHA DE MATRICULA
					datediff (yy,mt_client.fecnac,getdate()) as edad,				--EDAD
					mt_client.sexo,													--GENERO
					mt_client.nacionalidad,											--NACIONALIDAD
					mt_client.diractual as direccion_actual,
					mt_client.comuna as comuna,
					mt_client.ciudadact ciudad,
					mt_client.fonoact as fono,
					mt_client.mail,												--
					mt_alumno.ano as cohorte,												--AÑO INGRESO
					rtrim(ltrim(mt_alumno.estacad)) as estado_academico ,				--ESTADO ACADEMICO
					ra_tipositu.descripcion as descripcon_estado_academico,						--DESCRIPCION
					mt_alumno.codpestud as plan_estudio,

					CONVERT(VARCHAR(10), mt_alumno.fec_mat, 103) as fecha_matricula,
					mt_alumno.ano_mat,
					replace(replace(replace(matricula.fn_glosa_ingles(mt_alumno.codcli) ,char(10),' '),char(9),' '),char(13),' ') as TEST_INGLES,
					t_test_idioma.codramo as test_idioma,
					t_ing_1.codramo + ' (' + t_ing_1.ESTADO + ' )' as ingles_1, 
					t_ing_2.codramo + ' (' + t_ing_2.ESTADO + ' )' as ingles_2,
					t_ing_3.codramo + ' (' + t_ing_3.ESTADO + ' )' as ingles_3


					FROM mt_alumno with (noLOCK) INNER JOIN mt_client with (noLOCK) ON mt_alumno.rut=mt_client.codcli
					INNER JOIN mt_carrer with (noLOCK) on mt_alumno.codcarpr=mt_carrer.codcarr
					left outer join mt_tipocarr on mt_carrer.tipocarr=mt_tipocarr.tipocarr
					left outer join ra_facultad with (noLOCK)		on mt_carrer.codfac	= ra_facultad.codfac
					inner JOIN ra_tipositu with (noLOCK) ON mt_alumno.tipositu=ra_tipositu.codigo
					left outer join (select ra_nota.codcli, ra_nota.codramo
									 from ra_nota 
									 inner join ra_ramo on ra_nota.codramo=ra_ramo.codramo
									 where ra_nota.ramoequiv='TDI-CT-01'
									-- and ra_nota.estado in ('A','E','I')
									 ) t_test_idioma on mt_alumno.codcli=t_test_idioma.codcli

					left outer join (select ra_nota.codcli, ra_nota.codramo, ra_nota.estado
					 				 from ra_nota 
									 inner join ra_ramo on ra_nota.codramo=ra_ramo.codramo
									 INNER JOIN (	 SELECT RA_NOTA.CODCLI, RA_NOTA.RAMOEQUIV,RA_NOTA.ANO, MAX(RA_NOTA.PERIODO) AS PERIODO
													 FROM RA_NOTA
													 INNER JOIN (
																 SELECT RA_NOTA.CODCLI, RA_NOTA.RAMOEQUIV, MAX(RA_NOTA.ANO) AS ANO
																 FROM RA_NOTA
																 WHERE RA_NOTA.RAMOEQUIV='ING-1'
																 GROUP BY RA_NOTA.CODCLI,RA_NOTA.RAMOEQUIV

														) T_MAX_ANO ON RA_NOTA.CODCLI=T_MAX_ANO.CODCLI
																	   AND RA_NOTA.RAMOEQUIV=T_MAX_ANO.RAMOEQUIV
																	   AND RA_NOTA.ANO=T_MAX_ANO.ANO
												

													 WHERE RA_NOTA.RAMOEQUIV='ING-1'
													 GROUP BY RA_NOTA.CODCLI,RA_NOTA.RAMOEQUIV, RA_NOTA.ANO
													) T_PERIODO ON RA_NOTA.CODCLI = T_PERIODO.CODCLI
													AND RA_NOTA.RAMOEQUIV = T_PERIODO.RAMOEQUIV
													AND RA_NOTA.ANO = T_PERIODO.ANO
													AND RA_NOTA.PERIODO = T_PERIODO.PERIODO
									 where ra_nota.ramoequiv='ING-1'
									 --and ra_nota.estado in ('A','E','I')
									 ) t_ing_1 on mt_alumno.codcli=t_ing_1.codcli


					left outer join (select ra_nota.codcli, ra_nota.codramo, RA_NOTA.ESTADO
									 from ra_nota 
									 inner join ra_ramo on ra_nota.codramo=ra_ramo.codramo
									 INNER JOIN (	 SELECT RA_NOTA.CODCLI, RA_NOTA.RAMOEQUIV,RA_NOTA.ANO, MAX(RA_NOTA.PERIODO) AS PERIODO
													 FROM RA_NOTA
													 INNER JOIN (
																 SELECT RA_NOTA.CODCLI, RA_NOTA.RAMOEQUIV, MAX(RA_NOTA.ANO) AS ANO
																 FROM RA_NOTA
																 WHERE RA_NOTA.RAMOEQUIV='ING-2'
																 GROUP BY RA_NOTA.CODCLI,RA_NOTA.RAMOEQUIV

														) T_MAX_ANO ON RA_NOTA.CODCLI=T_MAX_ANO.CODCLI
																	   AND RA_NOTA.RAMOEQUIV=T_MAX_ANO.RAMOEQUIV
																	   AND RA_NOTA.ANO=T_MAX_ANO.ANO

													 WHERE RA_NOTA.RAMOEQUIV='ING-2'
													 GROUP BY RA_NOTA.CODCLI,RA_NOTA.RAMOEQUIV, RA_NOTA.ANO
													) T_PERIODO ON RA_NOTA.CODCLI = T_PERIODO.CODCLI
													AND RA_NOTA.RAMOEQUIV = T_PERIODO.RAMOEQUIV
													AND RA_NOTA.ANO = T_PERIODO.ANO
													AND RA_NOTA.PERIODO = T_PERIODO.PERIODO
									 where ra_nota.ramoequiv='ING-2'
									 --and ra_nota.estado in ('A','E','I')
									 ) t_ing_2 on mt_alumno.codcli=t_ing_2.codcli

					left outer join (select ra_nota.codcli, ra_nota.codramo, RA_NOTA.ESTADO
									 from ra_nota 
									 inner join ra_ramo on ra_nota.codramo=ra_ramo.codramo
									 INNER JOIN (	 SELECT RA_NOTA.CODCLI, RA_NOTA.RAMOEQUIV,RA_NOTA.ANO, MAX(RA_NOTA.PERIODO) AS PERIODO
													 FROM RA_NOTA
													 INNER JOIN (
																 SELECT RA_NOTA.CODCLI, RA_NOTA.RAMOEQUIV, MAX(RA_NOTA.ANO) AS ANO
																 FROM RA_NOTA
																 WHERE RA_NOTA.RAMOEQUIV='ING-3'
																 GROUP BY RA_NOTA.CODCLI,RA_NOTA.RAMOEQUIV

														) T_MAX_ANO ON RA_NOTA.CODCLI=T_MAX_ANO.CODCLI
																	   AND RA_NOTA.RAMOEQUIV=T_MAX_ANO.RAMOEQUIV
																	   AND RA_NOTA.ANO=T_MAX_ANO.ANO

													 WHERE RA_NOTA.RAMOEQUIV='ING-3'
													 GROUP BY RA_NOTA.CODCLI,RA_NOTA.RAMOEQUIV, RA_NOTA.ANO
													) T_PERIODO ON RA_NOTA.CODCLI = T_PERIODO.CODCLI
													AND RA_NOTA.RAMOEQUIV = T_PERIODO.RAMOEQUIV
													AND RA_NOTA.ANO = T_PERIODO.ANO
													AND RA_NOTA.PERIODO = T_PERIODO.PERIODO
									 --and ra_nota.estado in ('A','E','I')
									 where ra_nota.ramoequiv='ING-3'
									 ) t_ing_3 on mt_alumno.codcli=t_ing_3.codcli

					where mt_alumno.ano>='2017'
					--and mt_carrer.tipocarr='1'


					order by mt_carrer.nombre_l, mt_client.paterno, mt_client.materno, mt_client.nombre
				"""
		return sql
