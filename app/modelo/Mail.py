from  app import config #archivo de configuración de la app

from email import encoders                                                                       
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from os.path import basename
import smtplib
import ssl

class Mail:

	def __init__(self):

		self.remitente          = config.SMTP_USER
		self.password           = config.SMTP_PASS
		self.default_address    = [config.DAFAULT_ADDRESS]
		self.listaDestinatarios = config.LISTA_DESTINARIOS
		
	
		


	def enviar(self
		, asunto  ="UACH"#recibe un str con el texto del asunto
		, texto   =""    #recibe un str con el texto del mensaje
		, archivos= None #recibe una lista con la rura de los archivos
		, destino  =None #recibe una lista de destinos
		):

	    
	    
	    if destino==None and config._APP_ENV!="TEST":
	    	destino =self.listaDestinatarios

	    if(config._APP_ENV=="TEST"):
	    	destino =[config.DAFAULT_ADDRESS]

	    msg = MIMEMultipart()
	    msg['From'] = self.remitente
	    msg['To'] = ', '.join(destino)
	    msg['Subject'] = asunto

	    
	    msg.attach(MIMEText(texto))
	    #¿Para que enviar el texto en forma de archivo?
	    #msg.attach(MIMEText(texto, "plain"))

	    for f in archivos or []:
	        with open(f, "rb") as fil:
	            ext = f.split('.')[-1:]
	            attachedfile = MIMEApplication(fil.read(), _subtype = ext)
	            attachedfile.add_header(
	                'content-disposition', 'attachment', filename=basename(f) )
	        msg.attach(attachedfile)


	    smtp = smtplib.SMTP(
	                host = config.SMTP_SERVER,
	                port = config.SMTP_PORT
	            )
	    smtp.starttls(context=ssl.create_default_context())  
	    smtp.login(config.SMTP_USER,config.SMTP_PASS)
	    smtp.sendmail(self.remitente, destino, msg.as_string())
	    print("Mail enviado a:")
	    print(destino)