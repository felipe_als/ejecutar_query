from  app import config #archivo de configuración de la app
from openpyxl import Workbook #para crear excel
from openpyxl.utils import get_column_letter #para ajustar ancho de columna
import os #para crear directorio

class Excel:

	excel     = None #es el excel
	hoja      = None #es la hoja activa

	def __init__(self):

		if self.excel == None:#si no existe excel
			self.excel = Workbook()#crea uno nuevo
			self.hoja = self.excel.active#activa una hoja
			#wb = openpyxl.Workbook()
			


	def getHojaActiva(self):
		return self.hoja #retorna la hoja activa

	
	#guarda el excel creado en el directorio
	def crear(self,nombre=None):
		_nombre = "archivo.xlsx";#nombre por defecto

		if nombre!=None:
			_nombre =nombre+".xlsx"#si se ingreso un nombre se añade dicho nombre
		
		#for i, column_width in enumerate(column_widths):
    	#	worksheet.column_dimensions[get_column_letter(i+1)].width = column_widt

		#recorre las columnas para redimencionarlas
		MIN_WIDTH = 10#es el valor del ancho minimo
		for i, column_cells in enumerate(self.hoja.columns, start=1):
			width = (
				length
				if (length := max(len(str(cell_value) if (cell_value := cell.value) is not None else "")
					for cell in column_cells)) >= MIN_WIDTH
				else MIN_WIDTH
			)
			self.hoja.column_dimensions[get_column_letter(i)].width = width+2#asigna el ancho de la columna

		#FIN recorre las columnas para redimencionarlas
		try:
			os.mkdir(config._DOC_RUTA)#intenta crear la ruta, si ya existe lanza error
		except:
			pass #pasa de la excepción ya que si ocurrio un error es por que la ruta ya existe

		try:
			self.excel.save(config._DOC_RUTA+_nombre)#crea un excel en el directorio señalado en el config
		except Exception as e:
			print('No fue posible guardar el documento, es posible que ya exista y esté en uso, cierre el documento para continuar')
			print(e)#muestra el detalle del problema
		

		print("excel {} creado ".format(_nombre))
		return config._DOC_RUTA+_nombre

	#guarda el excel creado en el directorio indicado en el config
	def generar(self,listas=None,nombre=None,nombre_hoja=None):

		if listas==None:
			return

		self.hoja.title = "Reporte"
		if nombre_hoja!=None:
			self.hoja.title = nombre_hoja

		print("Creando reporte...")

		columna       =1 #determina la columna
		fila          =1 #determina la fila

		for columnas in listas:#recorre las filas
			for valor in columnas:#recorre las columnas de cada fila
				
				if valor == None:
					valor=""
				
				self.hoja.cell(row=fila, column=columna,value=str(valor))
				columna+=1


			columna =1 #determina la columna
			fila   +=1

		return self.crear(nombre)#guarda el excel creado en el directorio indicado en el config