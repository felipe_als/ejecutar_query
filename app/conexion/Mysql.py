import pymssql
from  app import config

class Mysql:

	
	con   = None #es la instancia de la conexion
	cursor= None #es el cursor 

	#retorna la conexion
	def __init__(self):
		try:
			self.conectar()
		except:
			print('No se pudo realizar la conexión')
		

		
	#crea una conexion
	def conectar(self): 
		if self.con == None :
			print ("Conectando...")
			try:

				self.con = pymssql.connect(server=config._MYSQ_HOT, user=config._MYSQ_HOTUSER, password=config._MYSQ_HOTPASS, database=config._MYSQ_DB)
				print ("Conectado ")

			except:

				print ("No se pudo conectar con SQLSERVER a {}".format(_MYSQ_HOT))

		return self.con

	#cierra la conexion
	def cerrar(self):
		if self.con == None :
			return
		self.con.close()

	#obtiene la conexion	
	def getConexion(self):
		return self.conectar()

	#ejecuta la consulta
	def setConsulta(self,sql):
		if self.con==None:#existe conexion
			return None#retorna nulo

		con = self.getConexion()#obtiene la conexion
		self.cursor = con.cursor()#inicia un cursor

		try:
			self.cursor.execute(sql) #ejecuta la consulta
			return True
		except Exception as e:
			print('posible problema con la sentencia sql')
			print(e)#muestra el detalle del problema
		return False		

	#ejecuta una consulta y cierra la conexion
	#retorna una lista con los resultados
	def get(self,sql=None):

		if sql==None or self.con==None:
			return None
		
		if not self.setConsulta(sql):#setea la consulta
			return None #si no logra hacer la consulta retorna none

		filas = self.cursor.fetchall()#obtiene los resultados de la consulta

		self.cursor.close()
		self.cerrar()

		return filas;

	#ejecuta una consulta y cierra la conexion
	#retorna una lista donde la primera fila es el nombre de la columna de la tabla
	def getConEncabezado(self,sql=None):

		if sql==None or self.con==None:
			return None
		
		if not self.setConsulta(sql):#setea la consulta
			return None #si no logra hacer la consulta retorna none

		columns = self.cursor.description
		#print(columns)
		colm=[]#es una lista que se convertira en tupla con el nombre de las columnas
		culumnas=tuple() #es una tupla con el nombre de las columnas
		#solo recorre las columnas
		for aux in columns:#recorre solo las culmnas
			colm.append(aux[0])

		culumnas=tuple(colm)
		filas = self.cursor.fetchall()#obtiene los resultados de la consulta

		filas[:0]=[culumnas] # agrega la tupla columna al principio de la lista

		self.cursor.close()
		self.cerrar()

		return filas;

	#ejecuta una consulta y cierra la conexion
	#retorna una lista de diccionarios con los datos obtenidos
	def getToDic(self,sql=None):

		if sql==None or self.con==None:
			return None
		
		if not self.setConsulta(sql):#setea la consulta
			return None #si no logra hacer la consulta retorna none

		
		columns = self.cursor.description
	
		result = []
		for value in self.cursor.fetchall():#recorre los resultados de la consulta
			tmp = {}
			for (index,column) in enumerate(value):
				tmp[columns[index][0]] = column
			result.append(tmp)
			#filas = curS.fetchall()



		self.cursor.close()
		self.cerrar()

		return result;